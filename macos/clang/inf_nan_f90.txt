Fortran: GNU Fortran (Homebrew GCC 10.2.0_2) 10.2.0
real operations
----------
 1.0 +  1.0 =  2.0    isnan F + F = F      isinf F + F = F      isnan_inf F + F = F  
 1.0 +  NaN =  NaN    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
 1.0 +  Inf =  Inf    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
 NaN +  1.0 =  NaN    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
 NaN +  NaN =  NaN    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
 NaN +  Inf =  NaN    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
 Inf +  1.0 =  Inf    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
 Inf +  NaN =  NaN    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
 Inf +  Inf =  Inf    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

----------
 1.0 *  1.0 =  1.0    isnan F * F = F      isinf F * F = F      isnan_inf F * F = F  
 1.0 *  NaN =  NaN    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
 1.0 *  Inf =  Inf    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
 NaN *  1.0 =  NaN    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
 NaN *  NaN =  NaN    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
 NaN *  Inf =  NaN    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
 Inf *  1.0 =  Inf    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
 Inf *  NaN =  NaN    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
 Inf *  Inf =  Inf    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  

----------
 1.0 /  1.0 =  1.0    isnan F / F = F      isinf F / F = F      isnan_inf F / F = F  
 1.0 /  NaN =  NaN    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
 1.0 /  Inf =  0.0    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
 NaN /  1.0 =  NaN    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
 NaN /  NaN =  NaN    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
 NaN /  Inf =  NaN    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
 Inf /  1.0 =  Inf    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
 Inf /  NaN =  NaN    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
 Inf /  Inf =  NaN    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

complex operations
----------
( 1.0,  1.0i) + ( 1.0,  1.0i) = ( 2.0,  2.0i)    isnan F + F = F      isinf F + F = F      isnan_inf F + F = F  
( 1.0,  1.0i) + ( 1.0,  NaNi) = ( 2.0,  NaNi)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( 1.0,  Infi) = ( 2.0,  Infi)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( NaN,  1.0i) = ( NaN,  2.0i)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( NaN,  Infi) = ( NaN,  Infi)    isnan F + T = T      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( Inf,  1.0i) = ( Inf,  2.0i)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( Inf,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  

( 1.0,  NaNi) + ( 1.0,  1.0i) = ( 2.0,  NaNi)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( 1.0,  NaNi) + ( 1.0,  NaNi) = ( 2.0,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  NaNi) + ( 1.0,  Infi) = ( 2.0,  NaNi)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( 1.0,  NaNi) + ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  NaNi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  NaNi) + ( NaN,  Infi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( 1.0,  NaNi) + ( Inf,  1.0i) = ( Inf,  NaNi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  
( 1.0,  NaNi) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan T + T = T      isinf F + T = T      isnan_inf T + T = T  
( 1.0,  NaNi) + ( Inf,  Infi) = ( Inf,  NaNi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  

( 1.0,  Infi) + ( 1.0,  1.0i) = ( 2.0,  Infi)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( 1.0,  Infi) + ( 1.0,  NaNi) = ( 2.0,  NaNi)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( 1.0,  Infi) + ( 1.0,  Infi) = ( 2.0,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  Infi) + ( NaN,  1.0i) = ( NaN,  Infi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( 1.0,  Infi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( 1.0,  Infi) + ( NaN,  Infi) = ( NaN,  Infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  Infi) + ( Inf,  1.0i) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  Infi) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  Infi) + ( Inf,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

( NaN,  1.0i) + ( 1.0,  1.0i) = ( NaN,  2.0i)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( NaN,  1.0i) + ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  1.0i) + ( 1.0,  Infi) = ( NaN,  Infi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  
( NaN,  1.0i) + ( NaN,  1.0i) = ( NaN,  2.0i)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  1.0i) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  1.0i) + ( NaN,  Infi) = ( NaN,  Infi)    isnan T + T = T      isinf F + T = T      isnan_inf T + T = T  
( NaN,  1.0i) + ( Inf,  1.0i) = ( NaN,  2.0i)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  1.0i) + ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  1.0i) + ( Inf,  Infi) = ( NaN,  Infi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  

( NaN,  NaNi) + ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( NaN,  NaNi) + ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  NaNi) + ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  NaNi) + ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  NaNi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( NaN,  NaNi) + ( NaN,  Infi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  NaNi) + ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  NaNi) + ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( NaN,  NaNi) + ( Inf,  Infi) = ( NaN,  NaNi)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  

( NaN,  Infi) + ( 1.0,  1.0i) = ( NaN,  Infi)    isnan T + F = T      isinf T + F = T      isnan_inf T + F = T  
( NaN,  Infi) + ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( NaN,  Infi) + ( 1.0,  Infi) = ( NaN,  Infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( NaN,  Infi) + ( NaN,  1.0i) = ( NaN,  Infi)    isnan T + T = T      isinf T + F = T      isnan_inf T + T = T  
( NaN,  Infi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( NaN,  Infi) + ( NaN,  Infi) = ( NaN,  Infi)    isnan T + T = T      isinf T + T = T      isnan_inf T + T = T  
( NaN,  Infi) + ( Inf,  1.0i) = ( NaN,  Infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( NaN,  Infi) + ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf T + T = F !    isnan_inf T + T = T  
( NaN,  Infi) + ( Inf,  Infi) = ( NaN,  Infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  

( Inf,  1.0i) + ( 1.0,  1.0i) = ( Inf,  2.0i)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( Inf,  1.0i) + ( 1.0,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( Inf,  1.0i) + ( 1.0,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( Inf,  1.0i) + ( NaN,  1.0i) = ( NaN,  2.0i)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( Inf,  1.0i) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( Inf,  1.0i) + ( NaN,  Infi) = ( NaN,  Infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  1.0i) + ( Inf,  1.0i) = ( Inf,  2.0i)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( Inf,  1.0i) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  1.0i) + ( Inf,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

( Inf,  NaNi) + ( 1.0,  1.0i) = ( Inf,  NaNi)    isnan T + F = T      isinf T + F = T      isnan_inf T + F = T  
( Inf,  NaNi) + ( 1.0,  NaNi) = ( Inf,  NaNi)    isnan T + T = T      isinf T + F = T      isnan_inf T + T = T  
( Inf,  NaNi) + ( 1.0,  Infi) = ( Inf,  NaNi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  NaNi) + ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( Inf,  NaNi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( Inf,  NaNi) + ( NaN,  Infi) = ( NaN,  NaNi)    isnan T + T = T      isinf T + T = F !    isnan_inf T + T = T  
( Inf,  NaNi) + ( Inf,  1.0i) = ( Inf,  NaNi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  NaNi) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan T + T = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  NaNi) + ( Inf,  Infi) = ( Inf,  NaNi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  

( Inf,  Infi) + ( 1.0,  1.0i) = ( Inf,  Infi)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( Inf,  Infi) + ( 1.0,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( Inf,  Infi) + ( 1.0,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( Inf,  Infi) + ( NaN,  1.0i) = ( NaN,  Infi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( Inf,  Infi) + ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( Inf,  Infi) + ( NaN,  Infi) = ( NaN,  Infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  Infi) + ( Inf,  1.0i) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( Inf,  Infi) + ( Inf,  NaNi) = ( Inf,  NaNi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( Inf,  Infi) + ( Inf,  Infi) = ( Inf,  Infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  


----------
( 1.0,  1.0i) * ( 1.0,  1.0i) = ( 0.0,  2.0i)    isnan F * F = F      isinf F * F = F      isnan_inf F * F = F  
( 1.0,  1.0i) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( 1.0,  Infi) = (-Inf,  Infi)    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan F * T = T      isinf F * T = F !    isnan_inf F * T = T  
( 1.0,  1.0i) * ( Inf,  1.0i) = ( Inf,  Infi)    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf F * T = F !    isnan_inf F * T = T  
( 1.0,  1.0i) * ( Inf,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf F * T = T      isnan_inf F * T = T  

( 1.0,  NaNi) * ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( 1.0,  NaNi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  NaNi) * ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( 1.0,  NaNi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  NaNi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  NaNi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( 1.0,  NaNi) * ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( 1.0,  NaNi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( 1.0,  NaNi) * ( Inf,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  

( 1.0,  Infi) * ( 1.0,  1.0i) = (-Inf,  Infi)    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
( 1.0,  Infi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( 1.0,  Infi) * ( 1.0,  Infi) = (-Inf,  Infi)    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  
( 1.0,  Infi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( 1.0,  Infi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( 1.0,  Infi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( 1.0,  Infi) * ( Inf,  1.0i) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( 1.0,  Infi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( 1.0,  Infi) * ( Inf,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  

( NaN,  1.0i) * ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( NaN,  1.0i) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  1.0i) * ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  1.0i) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  1.0i) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  1.0i) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  1.0i) * ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  1.0i) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  1.0i) * ( Inf,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  

( NaN,  NaNi) * ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( NaN,  NaNi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  NaNi) * ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  NaNi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  NaNi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( NaN,  NaNi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  NaNi) * ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  NaNi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( NaN,  NaNi) * ( Inf,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  

( NaN,  Infi) * ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf T * F = F !    isnan_inf T * F = T  
( NaN,  Infi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * T = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * T = F !    isnan_inf T * T = T  
( NaN,  Infi) * ( Inf,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  

( Inf,  1.0i) * ( 1.0,  1.0i) = ( Inf,  Infi)    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
( Inf,  1.0i) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  1.0i) * ( 1.0,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( Inf,  1.0i) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  1.0i) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  1.0i) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  1.0i) * ( Inf,  1.0i) = ( Inf,  Infi)    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  
( Inf,  1.0i) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  1.0i) * ( Inf,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  

( Inf,  NaNi) * ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf T * F = F !    isnan_inf T * F = T  
( Inf,  NaNi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  NaNi) * ( Inf,  Infi) = ( NaN,  NaNi)    isnan T * F = T      isinf T * T = F !    isnan_inf T * T = T  

( Inf,  Infi) * ( 1.0,  1.0i) = ( NaN,  Infi)    isnan F * F = T !    isinf T * F = T      isnan_inf T * F = T  
( Inf,  Infi) * ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  Infi) * ( 1.0,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( Inf,  Infi) * ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  Infi) * ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( Inf,  Infi) * ( NaN,  Infi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  Infi) * ( Inf,  1.0i) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( Inf,  Infi) * ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F * T = T      isinf T * T = F !    isnan_inf T * T = T  
( Inf,  Infi) * ( Inf,  Infi) = ( NaN,  Infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  


----------
( 1.0,  1.0i) / ( 1.0,  1.0i) = ( 1.0,  0.0i)    isnan F / F = F      isinf F / F = F      isnan_inf F / F = F  
( 1.0,  1.0i) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( 1.0,  Infi) = ( 0.0, -0.0i)    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan F / T = T      isinf F / T = F !    isnan_inf F / T = T  
( 1.0,  1.0i) / ( Inf,  1.0i) = ( 0.0,  0.0i)    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf F / T = F !    isnan_inf F / T = T  
( 1.0,  1.0i) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf F / T = F !    isnan_inf F / T = T  

( 1.0,  NaNi) / ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( 1.0,  NaNi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  NaNi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  NaNi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  NaNi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  NaNi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  NaNi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  NaNi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  NaNi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( 1.0,  Infi) / ( 1.0,  1.0i) = ( Inf,  Infi)    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
( 1.0,  Infi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  Infi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

( NaN,  1.0i) / ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( NaN,  1.0i) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  1.0i) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  1.0i) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  1.0i) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  1.0i) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  1.0i) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  1.0i) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  1.0i) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( NaN,  NaNi) / ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( NaN,  NaNi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  NaNi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  NaNi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  NaNi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( NaN,  NaNi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  NaNi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  NaNi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( NaN,  NaNi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( NaN,  Infi) / ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf T / F = F !    isnan_inf T / F = T  
( NaN,  Infi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( NaN,  Infi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  

( Inf,  1.0i) / ( 1.0,  1.0i) = ( Inf, -Infi)    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
( Inf,  1.0i) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  1.0i) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

( Inf,  NaNi) / ( 1.0,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf T / F = F !    isnan_inf T / F = T  
( Inf,  NaNi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  NaNi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  

( Inf,  Infi) / ( 1.0,  1.0i) = ( Inf,  NaNi)    isnan F / F = T !    isinf T / F = T      isnan_inf T / F = T  
( Inf,  Infi) / ( 1.0,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( 1.0,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( NaN,  1.0i) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( NaN,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( NaN,  Infi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( Inf,  1.0i) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( Inf,  NaNi) = ( NaN,  NaNi)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( Inf,  Infi) / ( Inf,  Infi) = ( NaN,  NaNi)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  


! indicates (isnan|isinf|isnan_inf) of inputs != (isnan|isinf|isnan_inf) of output
