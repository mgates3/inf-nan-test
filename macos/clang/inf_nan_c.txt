C: Apple clang version 12.0.0 (clang-1200.0.32.2)
real operations
----------
 1.0 +  1.0 =  2.0    isnan F + F = F      isinf F + F = F      isnan_inf F + F = F  
 1.0 +  nan =  nan    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
 1.0 +  inf =  inf    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
 nan +  1.0 =  nan    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
 nan +  nan =  nan    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
 nan +  inf =  nan    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
 inf +  1.0 =  inf    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
 inf +  nan =  nan    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
 inf +  inf =  inf    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

----------
 1.0 *  1.0 =  1.0    isnan F * F = F      isinf F * F = F      isnan_inf F * F = F  
 1.0 *  nan =  nan    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
 1.0 *  inf =  inf    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
 nan *  1.0 =  nan    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
 nan *  nan =  nan    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
 nan *  inf =  nan    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
 inf *  1.0 =  inf    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
 inf *  nan =  nan    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
 inf *  inf =  inf    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  

----------
 1.0 /  1.0 =  1.0    isnan F / F = F      isinf F / F = F      isnan_inf F / F = F  
 1.0 /  nan =  nan    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
 1.0 /  inf =  0.0    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
 nan /  1.0 =  nan    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
 nan /  nan =  nan    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
 nan /  inf =  nan    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
 inf /  1.0 =  inf    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
 inf /  nan =  nan    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
 inf /  inf =  nan    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

complex operations
----------
( 1.0,  1.0i) + ( 1.0,  1.0i) = ( 2.0,  2.0i)    isnan F + F = F      isinf F + F = F      isnan_inf F + F = F  
( 1.0,  1.0i) + ( 1.0,  nani) = ( 2.0,  nani)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( 1.0,  infi) = ( 2.0,  infi)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( nan,  1.0i) = ( nan,  2.0i)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( nan,  nani) = ( nan,  nani)    isnan F + T = T      isinf F + F = F      isnan_inf F + T = T  
( 1.0,  1.0i) + ( nan,  infi) = ( nan,  infi)    isnan F + T = T      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( inf,  1.0i) = ( inf,  2.0i)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( inf,  nani) = ( inf,  nani)    isnan F + T = T      isinf F + T = T      isnan_inf F + T = T  
( 1.0,  1.0i) + ( inf,  infi) = ( inf,  infi)    isnan F + F = F      isinf F + T = T      isnan_inf F + T = T  

( 1.0,  nani) + ( 1.0,  1.0i) = ( 2.0,  nani)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( 1.0,  nani) + ( 1.0,  nani) = ( 2.0,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  nani) + ( 1.0,  infi) = ( 2.0,  nani)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( 1.0,  nani) + ( nan,  1.0i) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  nani) + ( nan,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( 1.0,  nani) + ( nan,  infi) = ( nan,  nani)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( 1.0,  nani) + ( inf,  1.0i) = ( inf,  nani)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  
( 1.0,  nani) + ( inf,  nani) = ( inf,  nani)    isnan T + T = T      isinf F + T = T      isnan_inf T + T = T  
( 1.0,  nani) + ( inf,  infi) = ( inf,  nani)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  

( 1.0,  infi) + ( 1.0,  1.0i) = ( 2.0,  infi)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( 1.0,  infi) + ( 1.0,  nani) = ( 2.0,  nani)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( 1.0,  infi) + ( 1.0,  infi) = ( 2.0,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  infi) + ( nan,  1.0i) = ( nan,  infi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( 1.0,  infi) + ( nan,  nani) = ( nan,  nani)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( 1.0,  infi) + ( nan,  infi) = ( nan,  infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  infi) + ( inf,  1.0i) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  infi) + ( inf,  nani) = ( inf,  nani)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( 1.0,  infi) + ( inf,  infi) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

( nan,  1.0i) + ( 1.0,  1.0i) = ( nan,  2.0i)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( nan,  1.0i) + ( 1.0,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  1.0i) + ( 1.0,  infi) = ( nan,  infi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  
( nan,  1.0i) + ( nan,  1.0i) = ( nan,  2.0i)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  1.0i) + ( nan,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  1.0i) + ( nan,  infi) = ( nan,  infi)    isnan T + T = T      isinf F + T = T      isnan_inf T + T = T  
( nan,  1.0i) + ( inf,  1.0i) = ( nan,  2.0i)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  1.0i) + ( inf,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  1.0i) + ( inf,  infi) = ( nan,  infi)    isnan T + F = T      isinf F + T = T      isnan_inf T + T = T  

( nan,  nani) + ( 1.0,  1.0i) = ( nan,  nani)    isnan T + F = T      isinf F + F = F      isnan_inf T + F = T  
( nan,  nani) + ( 1.0,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  nani) + ( 1.0,  infi) = ( nan,  nani)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  nani) + ( nan,  1.0i) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  nani) + ( nan,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + F = F      isnan_inf T + T = T  
( nan,  nani) + ( nan,  infi) = ( nan,  nani)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  nani) + ( inf,  1.0i) = ( nan,  nani)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  nani) + ( inf,  nani) = ( nan,  nani)    isnan T + T = T      isinf F + T = F !    isnan_inf T + T = T  
( nan,  nani) + ( inf,  infi) = ( nan,  nani)    isnan T + F = T      isinf F + T = F !    isnan_inf T + T = T  

( nan,  infi) + ( 1.0,  1.0i) = ( nan,  infi)    isnan T + F = T      isinf T + F = T      isnan_inf T + F = T  
( nan,  infi) + ( 1.0,  nani) = ( nan,  nani)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( nan,  infi) + ( 1.0,  infi) = ( nan,  infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( nan,  infi) + ( nan,  1.0i) = ( nan,  infi)    isnan T + T = T      isinf T + F = T      isnan_inf T + T = T  
( nan,  infi) + ( nan,  nani) = ( nan,  nani)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( nan,  infi) + ( nan,  infi) = ( nan,  infi)    isnan T + T = T      isinf T + T = T      isnan_inf T + T = T  
( nan,  infi) + ( inf,  1.0i) = ( nan,  infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( nan,  infi) + ( inf,  nani) = ( nan,  nani)    isnan T + T = T      isinf T + T = F !    isnan_inf T + T = T  
( nan,  infi) + ( inf,  infi) = ( nan,  infi)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  

( inf,  1.0i) + ( 1.0,  1.0i) = ( inf,  2.0i)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( inf,  1.0i) + ( 1.0,  nani) = ( inf,  nani)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( inf,  1.0i) + ( 1.0,  infi) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( inf,  1.0i) + ( nan,  1.0i) = ( nan,  2.0i)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( inf,  1.0i) + ( nan,  nani) = ( nan,  nani)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( inf,  1.0i) + ( nan,  infi) = ( nan,  infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  1.0i) + ( inf,  1.0i) = ( inf,  2.0i)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( inf,  1.0i) + ( inf,  nani) = ( inf,  nani)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  1.0i) + ( inf,  infi) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  

( inf,  nani) + ( 1.0,  1.0i) = ( inf,  nani)    isnan T + F = T      isinf T + F = T      isnan_inf T + F = T  
( inf,  nani) + ( 1.0,  nani) = ( inf,  nani)    isnan T + T = T      isinf T + F = T      isnan_inf T + T = T  
( inf,  nani) + ( 1.0,  infi) = ( inf,  nani)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  nani) + ( nan,  1.0i) = ( nan,  nani)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( inf,  nani) + ( nan,  nani) = ( nan,  nani)    isnan T + T = T      isinf T + F = F !    isnan_inf T + T = T  
( inf,  nani) + ( nan,  infi) = ( nan,  nani)    isnan T + T = T      isinf T + T = F !    isnan_inf T + T = T  
( inf,  nani) + ( inf,  1.0i) = ( inf,  nani)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  nani) + ( inf,  nani) = ( inf,  nani)    isnan T + T = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  nani) + ( inf,  infi) = ( inf,  nani)    isnan T + F = T      isinf T + T = T      isnan_inf T + T = T  

( inf,  infi) + ( 1.0,  1.0i) = ( inf,  infi)    isnan F + F = F      isinf T + F = T      isnan_inf T + F = T  
( inf,  infi) + ( 1.0,  nani) = ( inf,  nani)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( inf,  infi) + ( 1.0,  infi) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( inf,  infi) + ( nan,  1.0i) = ( nan,  infi)    isnan F + T = T      isinf T + F = T      isnan_inf T + T = T  
( inf,  infi) + ( nan,  nani) = ( nan,  nani)    isnan F + T = T      isinf T + F = F !    isnan_inf T + T = T  
( inf,  infi) + ( nan,  infi) = ( nan,  infi)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  infi) + ( inf,  1.0i) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  
( inf,  infi) + ( inf,  nani) = ( inf,  nani)    isnan F + T = T      isinf T + T = T      isnan_inf T + T = T  
( inf,  infi) + ( inf,  infi) = ( inf,  infi)    isnan F + F = F      isinf T + T = T      isnan_inf T + T = T  


----------
( 1.0,  1.0i) * ( 1.0,  1.0i) = ( 0.0,  2.0i)    isnan F * F = F      isinf F * F = F      isnan_inf F * F = F  
( 1.0,  1.0i) * ( 1.0,  nani) = ( nan,  nani)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( 1.0,  infi) = (-inf,  infi)    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( nan,  1.0i) = ( nan,  nani)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( nan,  nani) = ( nan,  nani)    isnan F * T = T      isinf F * F = F      isnan_inf F * T = T  
( 1.0,  1.0i) * ( nan,  infi) = (-inf,  infi)    isnan F * T = F !    isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( inf,  1.0i) = ( inf,  infi)    isnan F * F = F      isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( inf,  nani) = ( inf,  infi)    isnan F * T = F !    isinf F * T = T      isnan_inf F * T = T  
( 1.0,  1.0i) * ( inf,  infi) = ( nan,  infi)    isnan F * F = T !    isinf F * T = T      isnan_inf F * T = T  

( 1.0,  nani) * ( 1.0,  1.0i) = ( nan,  nani)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( 1.0,  nani) * ( 1.0,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  nani) * ( 1.0,  infi) = ( nan,  infi)    isnan T * F = T      isinf F * T = T      isnan_inf T * T = T  
( 1.0,  nani) * ( nan,  1.0i) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  nani) * ( nan,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( 1.0,  nani) * ( nan,  infi) = ( nan,  infi)    isnan T * T = T      isinf F * T = T      isnan_inf T * T = T  
( 1.0,  nani) * ( inf,  1.0i) = ( inf,  nani)    isnan T * F = T      isinf F * T = T      isnan_inf T * T = T  
( 1.0,  nani) * ( inf,  nani) = ( inf,  nani)    isnan T * T = T      isinf F * T = T      isnan_inf T * T = T  
( 1.0,  nani) * ( inf,  infi) = ( inf,  infi)    isnan T * F = F !    isinf F * T = T      isnan_inf T * T = T  

( 1.0,  infi) * ( 1.0,  1.0i) = (-inf,  infi)    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
( 1.0,  infi) * ( 1.0,  nani) = ( nan,  infi)    isnan F * T = T      isinf T * F = T      isnan_inf T * T = T  
( 1.0,  infi) * ( 1.0,  infi) = (-inf,  infi)    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  
( 1.0,  infi) * ( nan,  1.0i) = (-inf,  nani)    isnan F * T = T      isinf T * F = T      isnan_inf T * T = T  
( 1.0,  infi) * ( nan,  nani) = ( nan,  nani)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( 1.0,  infi) * ( nan,  infi) = (-inf,  nani)    isnan F * T = T      isinf T * T = T      isnan_inf T * T = T  
( 1.0,  infi) * ( inf,  1.0i) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( 1.0,  infi) * ( inf,  nani) = ( nan,  infi)    isnan F * T = T      isinf T * T = T      isnan_inf T * T = T  
( 1.0,  infi) * ( inf,  infi) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  

( nan,  1.0i) * ( 1.0,  1.0i) = ( nan,  nani)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( nan,  1.0i) * ( 1.0,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  1.0i) * ( 1.0,  infi) = (-inf,  nani)    isnan T * F = T      isinf F * T = T      isnan_inf T * T = T  
( nan,  1.0i) * ( nan,  1.0i) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  1.0i) * ( nan,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  1.0i) * ( nan,  infi) = (-inf,  nani)    isnan T * T = T      isinf F * T = T      isnan_inf T * T = T  
( nan,  1.0i) * ( inf,  1.0i) = ( nan,  infi)    isnan T * F = T      isinf F * T = T      isnan_inf T * T = T  
( nan,  1.0i) * ( inf,  nani) = ( nan,  infi)    isnan T * T = T      isinf F * T = T      isnan_inf T * T = T  
( nan,  1.0i) * ( inf,  infi) = (-inf,  infi)    isnan T * F = F !    isinf F * T = T      isnan_inf T * T = T  

( nan,  nani) * ( 1.0,  1.0i) = ( nan,  nani)    isnan T * F = T      isinf F * F = F      isnan_inf T * F = T  
( nan,  nani) * ( 1.0,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  nani) * ( 1.0,  infi) = ( nan,  nani)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( nan,  nani) * ( nan,  1.0i) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  nani) * ( nan,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * F = F      isnan_inf T * T = T  
( nan,  nani) * ( nan,  infi) = ( nan,  nani)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( nan,  nani) * ( inf,  1.0i) = ( nan,  nani)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  
( nan,  nani) * ( inf,  nani) = ( nan,  nani)    isnan T * T = T      isinf F * T = F !    isnan_inf T * T = T  
( nan,  nani) * ( inf,  infi) = ( nan,  nani)    isnan T * F = T      isinf F * T = F !    isnan_inf T * T = T  

( nan,  infi) * ( 1.0,  1.0i) = (-inf,  infi)    isnan T * F = F !    isinf T * F = T      isnan_inf T * F = T  
( nan,  infi) * ( 1.0,  nani) = ( nan,  infi)    isnan T * T = T      isinf T * F = T      isnan_inf T * T = T  
( nan,  infi) * ( 1.0,  infi) = (-inf,  nani)    isnan T * F = T      isinf T * T = T      isnan_inf T * T = T  
( nan,  infi) * ( nan,  1.0i) = (-inf,  nani)    isnan T * T = T      isinf T * F = T      isnan_inf T * T = T  
( nan,  infi) * ( nan,  nani) = ( nan,  nani)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( nan,  infi) * ( nan,  infi) = (-inf,  nani)    isnan T * T = T      isinf T * T = T      isnan_inf T * T = T  
( nan,  infi) * ( inf,  1.0i) = ( nan,  infi)    isnan T * F = T      isinf T * T = T      isnan_inf T * T = T  
( nan,  infi) * ( inf,  nani) = ( nan,  infi)    isnan T * T = T      isinf T * T = T      isnan_inf T * T = T  
( nan,  infi) * ( inf,  infi) = (-inf,  infi)    isnan T * F = F !    isinf T * T = T      isnan_inf T * T = T  

( inf,  1.0i) * ( 1.0,  1.0i) = ( inf,  infi)    isnan F * F = F      isinf T * F = T      isnan_inf T * F = T  
( inf,  1.0i) * ( 1.0,  nani) = ( inf,  nani)    isnan F * T = T      isinf T * F = T      isnan_inf T * T = T  
( inf,  1.0i) * ( 1.0,  infi) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( inf,  1.0i) * ( nan,  1.0i) = ( nan,  infi)    isnan F * T = T      isinf T * F = T      isnan_inf T * T = T  
( inf,  1.0i) * ( nan,  nani) = ( nan,  nani)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( inf,  1.0i) * ( nan,  infi) = ( nan,  infi)    isnan F * T = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  1.0i) * ( inf,  1.0i) = ( inf,  infi)    isnan F * F = F      isinf T * T = T      isnan_inf T * T = T  
( inf,  1.0i) * ( inf,  nani) = ( inf,  nani)    isnan F * T = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  1.0i) * ( inf,  infi) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  

( inf,  nani) * ( 1.0,  1.0i) = ( inf,  infi)    isnan T * F = F !    isinf T * F = T      isnan_inf T * F = T  
( inf,  nani) * ( 1.0,  nani) = ( inf,  nani)    isnan T * T = T      isinf T * F = T      isnan_inf T * T = T  
( inf,  nani) * ( 1.0,  infi) = ( nan,  infi)    isnan T * F = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  nani) * ( nan,  1.0i) = ( nan,  infi)    isnan T * T = T      isinf T * F = T      isnan_inf T * T = T  
( inf,  nani) * ( nan,  nani) = ( nan,  nani)    isnan T * T = T      isinf T * F = F !    isnan_inf T * T = T  
( inf,  nani) * ( nan,  infi) = ( nan,  infi)    isnan T * T = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  nani) * ( inf,  1.0i) = ( inf,  nani)    isnan T * F = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  nani) * ( inf,  nani) = ( inf,  nani)    isnan T * T = T      isinf T * T = T      isnan_inf T * T = T  
( inf,  nani) * ( inf,  infi) = ( inf,  infi)    isnan T * F = F !    isinf T * T = T      isnan_inf T * T = T  

( inf,  infi) * ( 1.0,  1.0i) = ( nan,  infi)    isnan F * F = T !    isinf T * F = T      isnan_inf T * F = T  
( inf,  infi) * ( 1.0,  nani) = ( inf,  infi)    isnan F * T = F !    isinf T * F = T      isnan_inf T * T = T  
( inf,  infi) * ( 1.0,  infi) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( inf,  infi) * ( nan,  1.0i) = (-inf,  infi)    isnan F * T = F !    isinf T * F = T      isnan_inf T * T = T  
( inf,  infi) * ( nan,  nani) = ( nan,  nani)    isnan F * T = T      isinf T * F = F !    isnan_inf T * T = T  
( inf,  infi) * ( nan,  infi) = (-inf,  infi)    isnan F * T = F !    isinf T * T = T      isnan_inf T * T = T  
( inf,  infi) * ( inf,  1.0i) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  
( inf,  infi) * ( inf,  nani) = ( inf,  infi)    isnan F * T = F !    isinf T * T = T      isnan_inf T * T = T  
( inf,  infi) * ( inf,  infi) = ( nan,  infi)    isnan F * F = T !    isinf T * T = T      isnan_inf T * T = T  


----------
( 1.0,  1.0i) / ( 1.0,  1.0i) = ( 1.0,  0.0i)    isnan F / F = F      isinf F / F = F      isnan_inf F / F = F  
( 1.0,  1.0i) / ( 1.0,  nani) = ( nan,  nani)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( 1.0,  infi) = ( 0.0, -0.0i)    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( nan,  1.0i) = ( nan,  nani)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( nan,  nani) = ( nan,  nani)    isnan F / T = T      isinf F / F = F      isnan_inf F / T = T  
( 1.0,  1.0i) / ( nan,  infi) = ( 0.0, -0.0i)    isnan F / T = F !    isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( inf,  1.0i) = ( 0.0,  0.0i)    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( inf,  nani) = ( 0.0,  0.0i)    isnan F / T = F !    isinf F / T = F !    isnan_inf F / T = F !
( 1.0,  1.0i) / ( inf,  infi) = ( 0.0,  0.0i)    isnan F / F = F      isinf F / T = F !    isnan_inf F / T = F !

( 1.0,  nani) / ( 1.0,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( 1.0,  nani) / ( 1.0,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  nani) / ( 1.0,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  nani) / ( nan,  1.0i) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  nani) / ( nan,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( 1.0,  nani) / ( nan,  infi) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  nani) / ( inf,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  nani) / ( inf,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( 1.0,  nani) / ( inf,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( 1.0,  infi) / ( 1.0,  1.0i) = ( inf,  infi)    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
( 1.0,  infi) / ( 1.0,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( 1.0,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( nan,  1.0i) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( nan,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( nan,  infi) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( inf,  1.0i) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( inf,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( 1.0,  infi) / ( inf,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

( nan,  1.0i) / ( 1.0,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( nan,  1.0i) / ( 1.0,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  1.0i) / ( 1.0,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  1.0i) / ( nan,  1.0i) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  1.0i) / ( nan,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  1.0i) / ( nan,  infi) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  1.0i) / ( inf,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  1.0i) / ( inf,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  1.0i) / ( inf,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( nan,  nani) / ( 1.0,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / F = F      isnan_inf T / F = T  
( nan,  nani) / ( 1.0,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  nani) / ( 1.0,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  nani) / ( nan,  1.0i) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  nani) / ( nan,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / F = F      isnan_inf T / T = T  
( nan,  nani) / ( nan,  infi) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  nani) / ( inf,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  nani) / ( inf,  nani) = ( nan,  nani)    isnan T / T = T      isinf F / T = F !    isnan_inf T / T = T  
( nan,  nani) / ( inf,  infi) = ( nan,  nani)    isnan T / F = T      isinf F / T = F !    isnan_inf T / T = T  

( nan,  infi) / ( 1.0,  1.0i) = ( inf,  infi)    isnan T / F = F !    isinf T / F = T      isnan_inf T / F = T  
( nan,  infi) / ( 1.0,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( nan,  infi) / ( 1.0,  infi) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( nan,  infi) / ( nan,  1.0i) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( nan,  infi) / ( nan,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( nan,  infi) / ( nan,  infi) = ( nan,  nani)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( nan,  infi) / ( inf,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( nan,  infi) / ( inf,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( nan,  infi) / ( inf,  infi) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  

( inf,  1.0i) / ( 1.0,  1.0i) = ( inf, -infi)    isnan F / F = F      isinf T / F = T      isnan_inf T / F = T  
( inf,  1.0i) / ( 1.0,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( 1.0,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( nan,  1.0i) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( nan,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( nan,  infi) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( inf,  1.0i) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( inf,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  1.0i) / ( inf,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  

( inf,  nani) / ( 1.0,  1.0i) = ( inf, -infi)    isnan T / F = F !    isinf T / F = T      isnan_inf T / F = T  
( inf,  nani) / ( 1.0,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  nani) / ( 1.0,  infi) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  nani) / ( nan,  1.0i) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  nani) / ( nan,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  nani) / ( nan,  infi) = ( nan,  nani)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  nani) / ( inf,  1.0i) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  nani) / ( inf,  nani) = ( nan,  nani)    isnan T / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  nani) / ( inf,  infi) = ( nan,  nani)    isnan T / F = T      isinf T / T = F !    isnan_inf T / T = T  

( inf,  infi) / ( 1.0,  1.0i) = ( inf,  nani)    isnan F / F = T !    isinf T / F = T      isnan_inf T / F = T  
( inf,  infi) / ( 1.0,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  infi) / ( 1.0,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( inf,  infi) / ( nan,  1.0i) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  infi) / ( nan,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / F = F !    isnan_inf T / T = T  
( inf,  infi) / ( nan,  infi) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  infi) / ( inf,  1.0i) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  
( inf,  infi) / ( inf,  nani) = ( nan,  nani)    isnan F / T = T      isinf T / T = F !    isnan_inf T / T = T  
( inf,  infi) / ( inf,  infi) = ( nan,  nani)    isnan F / F = T !    isinf T / T = F !    isnan_inf T / T = T  


! indicates (isnan|isinf|isnan_inf) of inputs != (isnan|isinf|isnan_inf) of output
