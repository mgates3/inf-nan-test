#include <complex>

#include <stdio.h>
#include <math.h>

//------------------------------------------------------------------------------
template <typename T>
bool isnan( std::complex<T> x )
{
    return isnan( real(x) ) || isnan( imag(x) );
}

//------------------------------------------------------------------------------
template <typename T>
bool isinf( std::complex<T> x )
{
    return isinf( real(x) ) || isinf( imag(x) );
}

//------------------------------------------------------------------------------
template <typename T>
bool isnan_inf( T x )
{
    return isnan( x ) || isinf( x );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    using std::real;
    using std::imag;

    double nan_ = nan("");
    double inf_ = INFINITY;
    double val[] = { 1.0, nan_, inf_ };
    const int num_val = sizeof(val) / sizeof(*val);
    const int num_op = 3;

    printf( "C++: %s\n", VERSION );
    printf( "real operations\n" );
    for (int iop = 0; iop < num_op; ++iop) {
        printf( "----------\n" );
        for (int i0 = 0; i0 < num_val; ++i0) {
        for (int i1 = 0; i1 < num_val; ++i1) {
            double x = val[ i0 ];
            double y = val[ i1 ];
            double z;
            const char* op;
            switch (iop) {
                case 0: z = x + y; op = "+"; break;
                case 1: z = x * y; op = "*"; break;
                case 2: z = x / y; op = "/"; break;
            }
            printf( "%4.1f %s %4.1f = %4.1f"
                    "    isnan %c %s %c = %c %c"
                    "    isinf %c %s %c = %c %c"
                    "    isnan_inf %c %s %c = %c %c\n",
                    x, op, y, z,
                    isnan(x) ? 'T' : 'F', op,
                    isnan(y) ? 'T' : 'F',
                    isnan(z) ? 'T' : 'F',
                    (isnan(x) || isnan(y)) != isnan(z) ? '!' : ' ',
                    isinf(x) ? 'T' : 'F', op,
                    isinf(y) ? 'T' : 'F',
                    isinf(z) ? 'T' : 'F',
                    (isinf(x) || isinf(y)) != isinf(z) ? '!' : ' ',
                    isnan_inf(x) ? 'T' : 'F', op,
                    isnan_inf(y) ? 'T' : 'F',
                    isnan_inf(z) ? 'T' : 'F',
                    (isnan_inf(x) || isnan_inf(y)) != isnan_inf(z) ? '!' : ' ' );
        }
        }
        printf( "\n" );
    }

    printf( "complex operations\n" );
    for (int iop = 0; iop < num_op; ++iop) {
        printf( "----------\n" );
        for (int i0 = 0; i0 < num_val; ++i0) {
        for (int i1 = 0; i1 < num_val; ++i1) {
            std::complex<double> x( val[ i0 ], val[ i1 ] );
            for (int j0 = 0; j0 < num_val; ++j0) {
            for (int j1 = 0; j1 < num_val; ++j1) {
                std::complex<double> y( val[ j0 ], val[ j1 ] );

                std::complex<double> z;
                const char* op;
                switch (iop) {
                    case 0: z = x + y; op = "+"; break;
                    case 1: z = x * y; op = "*"; break;
                    case 2: z = x / y; op = "/"; break;
                }
                bool x_isnan = isnan(x);
                bool y_isnan = isnan(y);
                bool z_isnan = isnan(z);
                bool x_isinf = isinf(x);
                bool y_isinf = isinf(y);
                bool z_isinf = isinf(z);
                bool x_isnan_inf = isnan_inf(x);
                bool y_isnan_inf = isnan_inf(y);
                bool z_isnan_inf = isnan_inf(z);
                printf( "(%4.1f, %4.1fi) %s (%4.1f, %4.1fi) = (%4.1f, %4.1fi)"
                        "    isnan %c %s %c = %c %c"
                        "    isinf %c %s %c = %c %c"
                        "    isnan_inf %c %s %c = %c %c\n",
                        real(x), imag(x), op,
                        real(y), imag(y),
                        real(z), imag(z),
                        x_isnan ? 'T' : 'F', op,
                        y_isnan ? 'T' : 'F',
                        z_isnan ? 'T' : 'F',
                        (x_isnan || y_isnan) != z_isnan ? '!' : ' ',
                        x_isinf ? 'T' : 'F', op,
                        y_isinf ? 'T' : 'F',
                        z_isinf ? 'T' : 'F',
                        (x_isinf || y_isinf) != z_isinf ? '!' : ' ',
                        x_isnan_inf ? 'T' : 'F', op,
                        y_isnan_inf ? 'T' : 'F',
                        z_isnan_inf ? 'T' : 'F',
                        (x_isnan_inf || y_isnan_inf) != z_isnan_inf ? '!' : ' ' );
            }
            }
            printf( "\n" );
        }
        }
        printf( "\n" );
    }
    printf( "! indicates (isnan|isinf|isnan_inf) of inputs != (isnan|isinf|isnan_inf) of output\n" );

    return 0;
}
