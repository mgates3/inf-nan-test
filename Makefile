-include make.inc

src = ${wildcard *.cc *.c *.f90 *.F90}
obj = ${addsuffix .o, ${basename ${src}}}
exe = ${basename ${src}}
out = ${addsuffix .txt, ${exe}}

exe_c   = ${basename ${filter %.c,${src}}}
exe_cxx = ${basename ${filter %.cc,${src}}}
exe_f90 = ${basename ${filter %.f90 %.F90,${src}}}

CC_VERSION  = ${shell ${CC}  --version | head -n1}
CXX_VERSION = ${shell ${CXX} --version | head -n1}
FC_VERSION  = ${shell ${FC}  --version | head -n1}

CFLAGS   += -DVERSION='"${CC_VERSION}"'
CXXFLAGS += -DVERSION='"${CXX_VERSION}"'
FCFLAGS  += -DVERSION='"${FC_VERSION}"'

.SUFFIXES:
.SECONDARY:

all: ${out}

# Run exe to get output.
${out}: %.txt: %
	./$< > $@

# Link objects into exe.
${exe_c}: %: %.o
	${CC} ${LDFLAGS} -o $@ $^ ${LIBS}

${exe_cxx}: %: %.o
	${CXX} ${LDFLAGS} -o $@ $^ ${LIBS}

${exe_f90}: %: %.o
	${FC} ${LDFLAGS} -o $@ $^ ${LIBS}

# Compile objects.
%.o: %.c
	${CC} ${CFLAGS} -c -o $@ $<

%.o: %.cc
	${CXX} ${CXXFLAGS} -c -o $@ $<

%.o: %.f90
	${FC} ${FCFLAGS} -c -o $@ $<

%.o: %.F90
	${FC} ${FCFLAGS} -c -o $@ $<

# Utilities
clean:
	-rm -f ${exe} ${out} *.o *.d

# Adding blank lines makes the diff line-by-line instead of by chunk.
%_.txt: %.txt
	perl -pe 's/\n/\n\n/' $< > $@

diff: diff-c-cxx.txt diff-c-f90.txt diff-c-gnu-c.txt diff-f90-gnu-f90.txt

diff-c-cxx.txt: inf_nan_c_.txt inf_nan_cxx_.txt inf_nan_f90_.txt
	echo "diff -i inf_nan_c_.txt inf_nan_cxx_.txt" >  $@
	-     diff -i inf_nan_c_.txt inf_nan_cxx_.txt  | perl -pe 's/^---\n//' >> $@

diff-c-f90.txt: inf_nan_c_.txt inf_nan_cxx_.txt inf_nan_f90_.txt
	echo "diff -i inf_nan_c_.txt inf_nan_f90_.txt" >> $@
	-     diff -i inf_nan_c_.txt inf_nan_f90_.txt  | perl -pe 's/^---\n//'  >> $@

diff-c-gnu-c.txt: inf_nan_c_.txt inf_nan_cxx_.txt inf_nan_f90_.txt
	echo "diff -i inf_nan_c_.txt ../../saturn/gnu/inf_nan_c_.txt" >  $@
	-     diff -i inf_nan_c_.txt ../../saturn/gnu/inf_nan_c_.txt  | perl -pe 's/^---\n//' >> $@

diff-f90-gnu-f90.txt: inf_nan_c_.txt inf_nan_cxx_.txt inf_nan_f90_.txt
	echo "diff -i inf_nan_f90_.txt ../../saturn/gnu/inf_nan_f90_.txt" >> $@
	-     diff -i inf_nan_f90_.txt ../../saturn/gnu/inf_nan_f90_.txt  | perl -pe 's/^---\n//'  >> $@

echo:
	@echo "src ${src}"
	@echo "obj ${obj}"
	@echo "exe ${exe}"
	@echo "out ${out}"
	@echo "exe_c   ${exe_c}"
	@echo "exe_cxx ${exe_cxx}"
	@echo "exe_f90 ${exe_f90}"
	@echo "CC_VERSION  ${CC_VERSION}"
	@echo "CXX_VERSION ${CXX_VERSION}"
	@echo "FC_VERSION  ${FC_VERSION}"
